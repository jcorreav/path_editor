import PySide.QtCore as qtcore
import PySide.QtGui as qtgui

class PathDraw(qtgui.QWidget):
    clicked = qtcore.Signal(int, int, int)

    def __init__(self, parent):
        super(PathDraw, self).__init__(parent)
        self.time = 0

    def setMap(self, map_image, map_scale, map_origin):
        self.map_image = map_image
        self.map_scale = map_scale
        self.map_origin = map_origin
        self.update()

    def setSelectedPath(self, path_indx):
        self.selected_path = path_indx.row()
        self.update()

    def setPaths(self, paths):
        self.paths = paths

    def setTime(self, time):
        self.time = time
        self.update()

    def drawPathPoint(self, painter, path, point, selected = False):
        (x,y) = (point[1] / self.map_scale, point[2] / self.map_scale)
        path.lineTo(x, y)
        colour = qtgui.QColor(255, 0, 0) if not selected else qtgui.QColor(0, 0, 255)
        if point[0] == self.time:
            painter.fillRect(qtcore.QRect(x - 4, y - 4, 8, 8), qtgui.QBrush(colour))
        else:
            painter.drawRect(qtcore.QRect(x - 3, y - 3, 6, 6))

    def paintEvent(self, event):
        painter = qtgui.QPainter()
        painter.begin(self)
        if hasattr(self, 'map_image'):
            painter.drawPixmap(0, 0, self.map_image)
        # Draw the paths
        painter.setRenderHint(qtgui.QPainter.Antialiasing)
        if hasattr(self, 'paths') and len(self.paths) > 0:
            selected = self.selected_path if hasattr(self, 'selected_path') else -1
            for i,path in enumerate(self.paths):
                if len(path) > 0:
                    path_painter = qtgui.QPainterPath()
                    path_painter.moveTo(path[0][1] / self.map_scale, path[0][2] / self.map_scale)
                    self.drawPathPoint(painter, path_painter, path[0], selected == i)
                    for point in path[1:]:
                        self.drawPathPoint(painter, path_painter, point, selected == i)
                    painter.drawPath(path_painter)
        painter.end()

    def mousePressEvent(self, event):
        qtgui.QWidget.mousePressEvent(self, event)
        self.clicked.emit(event.button(), event.x(), event.y())
