import PySide.QtCore as qtcore

class PathList(qtcore.QAbstractListModel):
    def __init__(self, contents):
        super(PathList, self).__init__()
        self.contents = contents

    def rowCount(self, parent):
        return len(self.contents)

    def data(self, index, role):
        if role == qtcore.Qt.DisplayRole:
            return str("Path %d with %d points" % (index.row() +1, len(self.contents[index.row()])))

    def removeRow(self, firstRow, rowCount, parent=qtcore.QModelIndex()):
        self.beginRemoveRows(parent, firstRow, firstRow+rowCount-1)
        while rowCount > 0:
            del self.contents[firstRow]
            rowCount -= 1
        self.endRemoveRows()

    def addItem(self, item):
        self.beginInsertRows(qtcore.QModelIndex(), len(self.contents), len(self.contents))
        self.contents.append(item)
        self.endInsertRows()
