import PySide.QtGui as qtgui
import PySide.QtCore as qtcore
import path_editor_ui
from path_list import PathList
import yaml

class PathEditorWindow(qtgui.QMainWindow):
    def __init__(self, app, *kargs):
        # Create the application and main window
        qtgui.QMainWindow.__init__(self, *kargs)
        self.app = app

    def closeEvent(self, event):
        if self.app.ask_for_close():
            event.accept()
        else:
            event.ignore()
        
    @qtcore.Slot()
    def on_actionOpen_triggered(self):
        self.app.open_file()

    @qtcore.Slot()
    def on_actionSave_triggered(self):
        self.app.save_file()

    @qtcore.Slot()
    def on_actionLoadMap_triggered(self):
        self.app.load_map()

    @qtcore.Slot()
    def on_newPathButton_clicked(self):
        self.app.new_path()

    @qtcore.Slot()
    def on_deletePathButton_clicked(self):
        self.app.delete_paths()

    @qtcore.Slot()
    def on_deletePointPathButton_clicked(self):
        self.app.delete_point_path()

    @qtcore.Slot()
    def on_interpolateButton_clicked(self):
        self.app.interpolate()

    @qtcore.Slot()
    def on_mergePathsButton_clicked(self):
        self.app.merge_paths()

    @qtcore.Slot()
    def on_beginingButton_clicked(self):
        self.app.goto_begining()

    @qtcore.Slot()
    def on_endButton_clicked(self):
        self.app.goto_end()

    @qtcore.Slot()
    def on_backTimeButton_clicked(self):
        self.app.decreaseTime()

    @qtcore.Slot()
    def on_forwardTimeButton_clicked(self):
        self.app.increaseTime()

    @qtcore.Slot(int, int, int)
    def on_drawingArea_clicked(self, button, x, y):
        if button == 1:
            self.app.add_point(x, y)

class PathEditorApp(qtgui.QApplication):
    def __init__(self, *kargs):
        # Create the application and main window
        qtgui.QApplication.__init__(self, *kargs)
        self.main_window = PathEditorWindow(self)
        # Load the UI
        self.ui = path_editor_ui.Ui_pathEditor()
        self.ui.setupUi(self.main_window)
        # Show the window
        self.main_window.show()
        # Basic internal initialisation
        self.modified = False

    def ask_for_close(self):
        if hasattr(self, 'data') and self.modified:
            reply = qtgui.QMessageBox.question(self.main_window, 'All changes will be lost',
                        "Are you sure you want to quit?", qtgui.QMessageBox.Yes | qtgui.QMessageBox.No, qtgui.QMessageBox.No)
            if reply == qtgui.QMessageBox.No:
                return False
        return True
        
    def new_file(self):
        if hasattr(self, 'data') and self.modified:
            reply = qtgui.QMessageBox.question(self.main_window, 'All changes will be lost',
                        "Are you sure to create a new file?", qtgui.QMessageBox.Yes | qtgui.QMessageBox.No, qtgui.QMessageBox.No)
            if reply == qtgui.QMessageBox.No:
                return
        self.data = dict(filename='untitled.yaml', map=dict(), paths=[])
        self.path_list = PathList(self.data['paths'])
        self.ui.pathList.setModel(self.path_list)
        self.ui.drawingArea.setPaths(self.data['paths'])
        self.modified = True

    def open_file(self):
        filename, _ = qtgui.QFileDialog.getOpenFileName(self.main_window, 'Open File', '.', '*.yaml')
        if filename is None or len(filename) == 0:
            return
        data = yaml.load(open(filename, 'r'))

        # Check data
        if 'map' not in data or 'paths' not in data:
            raise RuntimeError('YAML file not complete')

        # Put data in app
        self.data = dict(
                        filename = filename,
                        map = data['map'],
                        paths = data['paths']
                        )

        self.load_map_image(self.data['map']['image'])
        self.modified = False 
        # Check if it is needed to fix the paths (old format x,y to new format x,y,vx,vy
        if len(self.data['paths']) > 0 and len(self.data['paths'][0][0]) < 5:
            # Complete with velocity /delta xy
            new_paths = []
            for path in self.data['paths']:
                prev = None
                new_path = []
                for i, point in enumerate(path):
                    dx = (point[1] - prev[1], point[2] - prev[2]) if i > 0 else (0,0)
                    if i == 1:
                        new_path.append((path[0][0], path[0][1], path[0][2], dx[0], dx[1]))
                    if i > 0:
                        new_path.append((point[0], point[1], point[2], dx[0], dx[1]))
                    prev = point
                new_paths.append(new_path)
            self.data['paths'] = new_paths
            self.modified = True
        # Set the path list display
        self.path_list = PathList(self.data['paths'])
        self.ui.pathList.setModel(self.path_list)
        self.ui.drawingArea.setPaths(self.data['paths'])

    def save_file(self):
        if hasattr(self, 'data') and self.modified:
            filename, _ = qtgui.QFileDialog.getOpenFileName(self.main_window, 'Save File', self.data['filename'], '*.yaml')
            self.data['filename'] = filename
            f = open(filename, 'w')
            f.write(yaml.dump(self.data))
            self.modified = False

    def load_map(self):
        filename, _ = qtgui.QFileDialog.getOpenFileName(self.main_window, 'Open File', '.', '*.yaml')
        if filename is None:
            return
        data = yaml.load(open(filename, 'r'))
        if not hasattr(self, 'data'):
            self.new_file()
        self.data['map'] = data
        self.load_map_image(self.data['map']['image'])
        self.modified = True

    def load_map_image(self, filename):
        self.mappixmap = qtgui.QPixmap()
        self.mappixmap.load(filename)
        self.ui.drawingArea.setMap(self.mappixmap, self.data['map']['resolution'], self.data['map']['origin'])
        
    def new_path(self):
        if not hasattr(self, 'data'):
            self.new_file()
        self.path_list.addItem([])
        self.modified = True

    def delete_paths(self):
        indices = self.ui.pathList.selectedIndexes()
        if len(indices) > 0:
            reply = qtgui.QMessageBox.question(self.main_window,
                        'Delete a path',
                        "Do you really want to delete this path?",
                        qtgui.QMessageBox.Yes | qtgui.QMessageBox.No,
                        qtgui.QMessageBox.No)
            if reply == qtgui.QMessageBox.No:
                return
            for i in indices:
                self.path_list.removeRow(i.row(), 1)
            self.modified = True

    def delete_point_path(self):
        indices = self.ui.pathList.selectedIndexes()
        time = int(self.ui.timestepLabel.text())
        for i in indices:
            for j, path in enumerate(self.data['paths'][i.row()]):
                if path[0] == time:
                    self.modified = True
                    del self.data['paths'][i.row()][j]
        self.ui.pathList.update()
        self.ui.drawingArea.update()

    def interpolate(self):
        import pycurve
        indices = self.ui.pathList.selectedIndexes()
        if len(indices) > 0:
            for i in indices:
                P = [(x,y) for (t,x,y) in self.data['paths'][i.row()]]
                t = [t for (t,x,y) in self.data['paths'][i.row()]]
                max_time = max(t)
                min_time = min(t)
                curve = pycurve.Lagrange(P,t)
                new_path = []
                for t in range(min_time, max_time+1):
                    x,y = curve(t)
                    new_path.append((t,x,y))
                self.data['paths'][i.row()] = new_path
            self.modified = True
        self.ui.drawingArea.update()

    def merge_paths(self):
        indices = self.ui.pathList.selectedIndexes()
        if len(indices) > 1:
            new_path = []
            times = []
            more_than_one_warning = False
            for i in indices:
                for p in self.data['paths'][i.row()]:
                    if p[0] in times:
                        if not more_than_one_warning:
                            reply = qtgui.QMessageBox.question(self.main_window,
                                        'The paths have point in a common time',
                                        "Merging the paths will lose some information. Are you sure to continue?",
                                        qtgui.QMessageBox.Yes | qtgui.QMessageBox.No,
                                        qtgui.QMessageBox.No)
                            if reply == qtgui.QMessageBox.No:
                                return
                        more_than_one_warning = True
                        continue
                    new_path.append(p)
                    times.append(p[0])
                self.path_list.removeRow(i.row(), 1)
            new_path = sorted(new_path, key=lambda x: x[0])
            self.path_list.addItem(new_path)
            self.modified = True

    def add_point(self, x, y):
        indices = self.ui.pathList.selectedIndexes()
        if len(indices) <> 1:
            return
        path_index = indices[0].row()
        time = int(self.ui.timestepLabel.text())
        map_scale = self.data['map']['resolution']
        if len(self.data['paths'][path_index]) == 0 or self.data['paths'][path_index][-1][0] < time:
            self.data['paths'][path_index].append((time, x * map_scale, y * map_scale))
            self.modified = True
        else:
            for i,step in enumerate(self.data['paths'][path_index]):
                if step[0] == time:
                    self.data['paths'][path_index][i] = (time, x * map_scale, y * map_scale)
                    self.modified = True
                    break
                elif step[0] > time:
                    self.data['paths'][path_index].insert(i, (time, x * map_scale, y * map_scale))
                    self.modified = True
                    break
        # To make thing easier, increase the time
        self.increaseTime()
        self.ui.drawingArea.update()

    def goto_begining(self):
        time = 0
        self.ui.timestepLabel.setText(str(time))
        self.ui.drawingArea.setTime(time)
        
    def decreaseTime(self):
        time = int(self.ui.timestepLabel.text())
        if time > 0:
            time -= 1
            self.ui.timestepLabel.setText(str(time))
            self.ui.drawingArea.setTime(time)

    def got_end(self):
        pass

    def increaseTime(self):
        time = int(self.ui.timestepLabel.text()) + 1
        self.ui.timestepLabel.setText(str(time))
        self.ui.drawingArea.setTime(time)

