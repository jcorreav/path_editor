#!/usr/bin/env python

if __name__ == "__main__":
    import sys
    import path_editor_app
    app = path_editor_app.PathEditorApp(sys.argv)

    app.exec_()
    sys.exit()
